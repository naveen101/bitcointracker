//
//  BPI.swift
//  BitcoinTracker
//
//  Created by Naveen on 16/08/21.
//  Copyright © 2021 Naveen. All rights reserved.
//

import Foundation
class BPI {
    var usd : USD?
    var gbp : GBP?
    var eur : EUR?
    init(dictionary: NSDictionary) {
        if let data = dictionary["USD"] as? [String:Any]{
            usd = USD(dictionary: data as NSDictionary)
        }
        if let data = dictionary["GBP"] as? [String:Any]{
            gbp = GBP(dictionary: data as NSDictionary)
        }
        if let data = dictionary["EUR"] as? [String:Any]{
            eur = EUR(dictionary: data as NSDictionary)
        }
    }
}

class USD {
    var code = ""
    var symbol = ""
    var rate = ""
    var description = ""
    var rate_float = Double()
    
    init(dictionary: NSDictionary) {
        code = dictionary["code"] as? String ?? ""
        symbol = dictionary["symbol"] as? String ?? ""
        rate = dictionary["rate"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        rate_float = dictionary["rate_float"] as? Double ?? 0.0
        
    }
}
class GBP {
    var code = ""
    var symbol = ""
    var rate = ""
    var description = ""
    var rate_float = Double()
    
    init(dictionary: NSDictionary) {
        code = dictionary["code"] as? String ?? ""
        symbol = dictionary["symbol"] as? String ?? ""
        rate = dictionary["rate"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        rate_float = dictionary["rate_float"] as? Double ?? 0.0
        
    }
}
class EUR {
    var code = ""
    var symbol = ""
    var rate = ""
    var description = ""
    var rate_float = Double()
    
    init(dictionary: NSDictionary) {
        code = dictionary["code"] as? String ?? ""
        symbol = dictionary["symbol"] as? String ?? ""
        rate = dictionary["rate"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        rate_float = dictionary["rate_float"] as? Double ?? 0.0
        
    }
}
