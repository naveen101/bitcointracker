//
//  Constant.swift
//  Gutenberg
//
//  Created by Naveen on 08/08/21.
//  Copyright © 2020 Naveen. All rights reserved.
//

import Foundation
import UIKit

@available(iOS 12.0, *)
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate


struct API {
    static let BASE_URL = "https://api.coindesk.com/"
    static let BPI = "v1/bpi/currentprice.json"
}
