//
//  ViewController.swift
//  BitcoinTracker
//
//  Created by Naveen on 16/08/21.
//  Copyright © 2021 Naveen. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIPickerViewDelegate{

    @IBOutlet weak var pickerVw: UIPickerView!
    @IBOutlet weak var priceLbl: UILabel!
    var currencyArray = [String]()
    var bpi : BPI?

    override func viewDidLoad() {
        super.viewDidLoad()
        currencyArray = [ "EUR","GBP","USD"]
        pickerVw.delegate = self
        pickerVw.dataSource = self
        getData()
    }
    
    
    func getData() {
        
        let URLString = "\(API.BASE_URL)\(API.BPI)"
        let url = URL(string: URLString)
        let session = URLSession.shared
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.url = url
        print("URL : \(URLString)")
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            //
            if error != nil
            {
                //print("ERROR : \(responseError)")
            }
            else {
                
                let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
                DispatchQueue.main.async(execute: { [self] in
                    //print("RESPONSE : \(jsonResponse)")
                    if let data = jsonResponse!["bpi"] as? [String:Any]{
                        self.bpi = BPI(dictionary: data as NSDictionary)
                    }
                    self.priceLbl.text = String((self.bpi?.eur!.rate_float)!)
                })
            }
        }
        dataTask.resume()
    }
}

extension ViewController : UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return currencyArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        let bpi = currencyArray[row]
        if bpi.lowercased() == "eur" {
            priceLbl.text = String((self.bpi?.eur!.rate_float)!)
        } else if bpi.lowercased() == "gbp" {
            priceLbl.text = String((self.bpi?.gbp!.rate_float)!)
        } else {
            priceLbl.text = String((self.bpi?.usd!.rate_float)!)
        }
    }
}
